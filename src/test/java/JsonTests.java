import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.infinitevoltage.sample.controller.JsonConverter;
import com.infinitevoltage.sample.implementations.GregDonarum;
import com.infinitevoltage.sample.implementations.Team;
import com.infinitevoltage.sample.model.MentorInterface;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

class JsonTests {

    @Test
    void mentorsTest() {
        Team team = new Team();
        assertEquals(team.getMentors().get(0).getFirstName(), "Greg");
    }

    @Test
    void mentorsJsonTest() {
        List<MentorInterface> mentors = new ArrayList<MentorInterface>();
        mentors.add(new GregDonarum());
        String json = "list error";
        try {
            json = JsonConverter.convertToJson(mentors);
        } catch (JsonProcessingException e1) {
            e1.printStackTrace();
        }
        String testJson = "read error";
        try {
            testJson = IOUtils.toString(this.getClass().getResourceAsStream("MentorListExample.json"), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(json, testJson);
    }
}