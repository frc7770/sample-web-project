package com.infinitevoltage.sample.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonConverter {
    
    private static final ObjectMapper mapper = new ObjectMapper();

    public static String convertToJson(Object input) throws JsonProcessingException {
        mapper.setConfig(mapper.getSerializationConfig().with(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY));
        return mapper.writeValueAsString(input);
    }
    
}
