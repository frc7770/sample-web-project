package com.infinitevoltage.sample.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.infinitevoltage.sample.implementations.Team;
 
public class TeamServlet extends HttpServlet {
 
    private static final long serialVersionUID = 1L;

    public static final String URL_ALL = "all.json";
    public static final String URL_STUDENTS = "students.json";
    public static final String URL_MENTORS = "mentors.json";

    // Initialize our team
    private static final Team team = new Team();
 
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }
    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      // the path that the web browser requested
      String path = req.getRequestURI();
      // take the last section of the path
      String[] folders = path.split("/");
      String listPath = "";
      if(folders.length>0)
        listPath = folders[folders.length-1];

      // initialize our output list
      String outputJson = "[]"; // empty array
      
      // Determin which list was requested
      switch (listPath) {
        case URL_STUDENTS:
          outputJson = JsonConverter.convertToJson(team.getStudents());
          break;
        case URL_MENTORS:
          outputJson = JsonConverter.convertToJson(team.getMentors());
          break;
        default:
          outputJson = JsonConverter.convertToJson(team.getAllMembers());
          break;
      }
  
      // return the JSON (JavaScript Object Notation)
      resp.getOutputStream().write(outputJson.getBytes());
    }
}