package com.infinitevoltage.sample.model;
/**
 * The definition of properties that mentors share.
 */
public interface MentorInterface extends TeamMemberInterface {

    /**
     * Employer
     * @return A String containing the mentor's employer if they have a job
     */
    public String getEmployer();

    /**
     * Job title
     *   example: Teacher
     * @return A String containing the mentor's job title.
     */
    public String getJobTitle();
}