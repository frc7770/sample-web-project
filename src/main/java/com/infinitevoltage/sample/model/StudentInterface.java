package com.infinitevoltage.sample.model;
/**
 * The definition of properties that students share.
 */
public interface StudentInterface extends TeamMemberInterface {

    /**
     * School
     * @return A String containing the school
     */
    public String getSchool();

    /**
     * Class
     *   example: Sophomore
     * @return A String containing the class.
     */
    public String getSchoolClass();
}