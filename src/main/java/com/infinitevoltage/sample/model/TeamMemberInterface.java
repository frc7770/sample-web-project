package com.infinitevoltage.sample.model;
/**
 * The definition of properties that all of our team members share.
 */
public interface TeamMemberInterface {

    /**
     * First name
     * @return A String containing the first name
     */
    public String getFirstName();

    /**
     * Last name
     * @return A String containing the last name
     */
    public String getLastName();

    /**
     * The first year that this person participated in robotics
     * @return An integer four digit year
     */
    public int getFirstYear();
}