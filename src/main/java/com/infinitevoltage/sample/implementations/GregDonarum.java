package com.infinitevoltage.sample.implementations;

import com.infinitevoltage.sample.model.MentorInterface;

/**
 * Hi, here's a little information about me.
 */
public class GregDonarum implements MentorInterface {

    // Mentor Implementations

    @Override
    public String getEmployer() {
        return "DCS Corp.";
    }

    @Override
    public String getJobTitle() {
        return "Software Engineer";
    }


    //Team Member Implementations

    @Override
    public String getFirstName() {
        return "Greg";
    }

    @Override
    public int getFirstYear() {
        return 2022;
    }

    @Override
    public String getLastName() {
        return "Donarum";
    }
    
}

