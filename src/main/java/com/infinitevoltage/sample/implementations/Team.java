package com.infinitevoltage.sample.implementations;

import java.util.ArrayList;
import java.util.List;

import com.infinitevoltage.sample.model.*;

/**
 * The Team is a class that holds our lista of members.
 */
public class Team {

    private List<TeamMemberInterface> allMembers = new ArrayList<TeamMemberInterface>();
    private List<StudentInterface> students = new ArrayList<StudentInterface>();
    private List<MentorInterface> mentors = new ArrayList<MentorInterface>();

    /**
     * The class constructor.  This is run when an instance of the class is initially created.
     * Create a Team and initialize its members.
     */
    public Team() {
        // Create our list of students.
        // **** Add new students here. ****
        students.add(new ExampleStudent());
        
        // Create our list of mentors.
        // **** Add new mentors here. ****
        mentors.add(new GregDonarum());

        // Add the two arrays above together to form the All list.
        // Since students and mentors extend the TeamMemberInterface they can exist in the same list of Team Members
        allMembers.addAll(students);
        allMembers.addAll(mentors);
    }


    /**
     * Expose a reference to the list of all team members
     * @return the List of everyone
     */
    public List<TeamMemberInterface> getAllMembers() {
        return allMembers;
    }


    /**
     * Expose a reference to the list of students
     * @return the student List
     */
    public List<StudentInterface> getStudents() {
        return students;
    }

    /**
     * Expose a reference to the list of mentors
     * @return the mentor List
     */
    public List<MentorInterface> getMentors() {
        return mentors;
    }
    
}
