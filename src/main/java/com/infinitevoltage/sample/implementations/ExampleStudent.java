package com.infinitevoltage.sample.implementations;

import com.infinitevoltage.sample.model.StudentInterface;

/**
 * An example of how to implement the interface definitions.
 * Note that StudentInterface extends Team MemberInterface so those methods are here too.
 */
public class ExampleStudent implements StudentInterface {

    // Team member inplementations

    @Override
    public String getFirstName() {
        return "Example";
    }

    @Override
    public String getLastName() {
        return "Student";
    }

    @Override
    public int getFirstYear() {
        return 2019;
    }


    // Student Implementations

    @Override
    public String getSchool() {
        return "C. Milton Wright";
    }

    @Override
    public String getSchoolClass() {
        return "Freshman";
    }
    
}
