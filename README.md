# Getting Started with Git and Java
This guide covers the basics of building Java programs and tracking your development with the Git source code management system.  This guide will not cover VSCode or any robotics specific information.

## Installing Git
Git is a free and open source SCM, or source code management, tool.  It allows you to save changes as you go, view the history of what has changed, and roll back or undo any edits that are not working out.

Git was originally developed by Linus Torvalds as a better way to track development of the Linux source code.  Since he manages such a large project, he wanted a tool that would let multiple teams develop concurrently and easily merge their work together as the community approved new features and bug fixes.

Git can be used to manage any folder on your computer.  If you want to backup that project to the cloud or share it with others, your local folder can be "pushed" to other repositories and your work can be "pulled" by other developers.  

This guide will focus on using Git from the command line as that is the best way to learn.  As you become comfortable you will notice that Git is built into most integrated development environments (IDE - VSCode, IntelliJ, Eclipse) and there are many visual tools (like TortoisGit).

First let's see if you already have Git.  First we need to start a command line tool.  On Windows, from the Start Menu, look for PowerShell or Command (aka the DOS prompt).  PowerShell is more powerful so we suggest using that.  In your favorite command line tool (PowerShell, DOS CMD, Linux shell) type in:
```
> git --version
git version 2.22.0.windows.1 (or similar)
```
If you see an actual version number than you are all set.  Otherwise we will need to install

### Installing on Windows
Download the Git installer from https://git-scm.com/

Run the installer and accept the defaults.  In addition to installing the git command to your machine, this installation will add a useful tool called Git Bash.  This is an additional command window tool like Powershell and DOS CMD that lets you use git and many other linux like commands.  

After installation run the "git --version" command again in Git Bash and your preferred command window to ensure that git is installed.

### Installing on Linux or the Linux Subsystem for Windows
```
# Ubuntu 
$ sudo apt install git-all

# CentOS
$ sudo yum install git-all

# After installation test by...
$ git --version
git version 2.17.1 (or similar)
```

### Clone this project
Now it's time to make a local copy of this project so that you can build, run, and change it.  The Git command we need to use is "clone". When you clone a project you get a local copy of its files and its entire git history.  The argument following "git clone" is the location of the source repository.  This is usually a URL pointeing to GitLab, GitHub, or some other system.

Let's run the clone command.  It will ask you to enter your GitLab username and password.  
```
> git clone https://gitlab.com/frc7770/sample-web-project.git
Cloning into 'sample-web-project'...
remote: Enumerating objects: 98, done.
remote: Counting objects: 100% (98/98), done.
remote: Compressing objects: 100% (78/78), done.
remote: Total 98 (delta 18), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (98/98), done.

> cd sample-web-project

> git status
On branch main
Your branch is up to date with 'origin/main'.

nothing to commit, working tree clean
```
Now you should have a sample-web-project folder containing all of the project files.  There is a hidden folder inside called ".git" that holds the git history.  Running "git status" shows information about your local copy of the folder and any edits that you have made.


## Running Simple Website example

- Run `./gradlew tomcatRun`
- Open in your browser:
  - http://localhost:8080/sample-website
- To stop...
  - Ctrl-C to kill the gradle process
  - Run './gradlew tomcatStop' to kill tomcat

If you receive the following error, than proceed to "Installing Java Development Kit"
```
ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation.
```

## Installing Java Development Kit (JDK)
Java is a popular object oriented programming language.  Java's two pieces are the Java Runtime Environment (JRE) and the Java Development Kit (JDK).  The JRE is used to execute java programs.  You probably already have it if you run Java programs like Minecraft.  The JDK is needed for those of us who want to create Java programs.  It has tools that allow you to compile and package your code into executable programs that can be run by the JRE.

There many flavors of Java from Microsoft, Oracle, and many open source projects.  We will use the Oracle version which can be downloaded here https://www.oracle.com/java/technologies/downloads .  Select the Windows 64 bit .EXE file.  Run the installer.  After completiong open a new command window and check that the install was successful.
```
> java --version
java 17.0.1 2021-10-19 LTS (or similar version)
Java(TM) SE Runtime Environment (build 17.0.1+12-LTS-39)
Java HotSpot(TM) 64-Bit Server VM (build 17.0.1+12-LTS-39, mixed mode, sharing)
```

## Build status

[![Build Status](https://travis-ci.org/gretty-gradle-plugin/gretty-sample.svg?branch=master)](https://travis-ci.org/gretty-gradle-plugin/gretty-sample)
